import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'user-info',
  styleUrls: [
    './user-info.component.scss'
  ],
  templateUrl: 'user-info.component.html'
})
export class UserInfoComponent {
  @Input('user') public user: User;
  @Output() public CloseUser = new EventEmitter<any>();

  public closeUser() {
    this.CloseUser.emit();
  }
}
