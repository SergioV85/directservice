import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UserService } from './../../services/user.service';

@Component({
  selector: 'users-list',
  styleUrls: [
    './users-list.component.scss'
  ],
  templateUrl: 'users-list.component.html'
})
export class UsersListComponent implements OnInit {
  @Output() public openUser = new EventEmitter<number>();
  public usersList: User[];

  constructor(private users: UserService) {
  }

  public ngOnInit() {
    this.users.UsersList.subscribe(
      (data) => {
        this.usersList = data;
      }
    );
  }

  public filterUsers(value) {
    this.users.filterUsers(value);
  }

  public selectUser(index) {
    this.openUser.emit(index);
  }
}
