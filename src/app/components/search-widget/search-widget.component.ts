import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'search-widget',
  styleUrls: [
    './search-widget.component.scss'
  ],
  templateUrl: 'search-widget.component.html'
})
export class SearchWidgetComponent {
  @Output() public onChange = new EventEmitter<string>();

  public filterUsers(value: string) {
    this.onChange.emit(value);
  }
}
