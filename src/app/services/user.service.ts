import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class UserService {
  public UsersList: Observable<User[]>;
  private usersListSource = new Subject<User[]>();

  private allUsers: User[];
  private userUrl = './assets/clients.json';

  constructor(private http: Http) {
    this.UsersList = this.usersListSource.asObservable();
    this.getUsersList();
  }

  public filterUsers(searchString: string) {
    const normalizedSearchString = searchString.toLowerCase();
    const filteredUsers = this.allUsers.filter((user) => {
      const subObj = this.toArray(user);
      const plainArray: string[] =
        subObj.reduce((concatedArray, currentArray) => [...concatedArray, ...currentArray], []);
      return plainArray.some((str) => str.toLowerCase().indexOf(normalizedSearchString) > -1);
    });

    this.usersListSource.next(filteredUsers);
  }
  public getUser(index) {
    return this.allUsers[index];
  }
  private getUsersList() {
    this.http
      .get(this.userUrl)
      .map(this.parseData)
      .catch(this.handleError)
      .subscribe(
        (data: User[]) => {
          this.allUsers = data;
          this.usersListSource.next(data);
        }
      );
  }
  private parseData(res: Response) {
    return res.json();
  }
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private toArray(obj) {
    const result = [];
    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        const value = obj[prop];
        if (typeof value === 'object') {
          result.push(this.toArray(value));
        } else {
          result.push(value);
        }
      }
    }
    return result;
  }
}
