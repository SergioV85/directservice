import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule, ApplicationRef } from '@angular/core';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { MaterialModule } from '@angular/material';

import { AppComponent } from './app.component';

import { UserService } from './services/user.service.ts';

import { UsersListComponent } from './components/users-list';
import { UserInfoComponent } from './components/user-info';
import { SearchWidgetComponent } from './components/search-widget';

import '../styles/styles.scss';
import '../styles/headings.css';

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    UsersListComponent,
    UserInfoComponent,
    SearchWidgetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot()
  ],
  providers: [
    UserService
  ]
})
export class AppModule {}
