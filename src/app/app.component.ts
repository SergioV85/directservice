import { Component } from '@angular/core';
import { UserService } from './services/user.service';

@Component({
  selector: 'app',
  styleUrls: [
    './app.component.scss'
  ],
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public selectedUser: User;

  constructor(private users: UserService){}

  public openUser(index) {
    this.selectedUser = this.users.getUser(index);
  }
  public closeUser() {
    this.selectedUser = null;
  }
}
